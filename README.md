# Good Migrations

A plain and simple migraton library for migrating anything you want.

You could write sql migrations, manipulate files, anything that javascript can do.

Version information is just a single number, and can be stored anywhere you like; in a database, filesystem, in the cloud, anywhere you have access to.

```javascript
const fs = require('fs');
const migrate = require('good-migrations');

migrate(
	// You need to specify how to read and write version data
	{
		async getVersion(){ return fs.readFileSync('./version', 'utf8'); },
		async setVersion(v){ fs.writeFileSync('./version', v); }
	},
	// Then give a list of migration functions to run
	[
		() => initiateWidgetManager(),
		async () => {
			const p = await calculateConfigurationParameter();
			await modifyConfigurationSetting(p);
		}
	]
).then(()=> console.log('done!'));
```

## SQL Migrations

Because it's such a common use case, an sql-specific migration system is also provided. It creates a table called `good_migrations` for recording migration status. This table name is not configurable, since there's no standard way to specify a variable as a table name in most sql clients.

It requires you to provide a function for executing SQL queries, which takes the signature of a template tagging function, so you'll probably want to use a library like `sql-template-tag` if your database client of choice doesn't provide template tags by default. The function should return a promise resolving to an array of result records (if there are any).

The second argument is a key to identify this set of migrations. It allows you to use `good-migrations` for multiple independent parts of your project.

Finally, the migration functions. You can also provide strings instead of functions and these will be interpreted as sql statements to be executed.

```javascript
const sql = require('sql-template-tag').default;
const migrate = require('good-migrations').sql;

const query = (literals, ...variables) => myDatabase.queryAll(sql(literals, ...variables));

migrate(
	query,
	'recipes table',
	[
		`CREATE TABLE recipes (title TEXT, body TEXT)`,
		()=>importRecipesFromFile()
	]
).then(()=>console.log('done!'))
```
