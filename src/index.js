const migrate = require('./migrate');
migrate.sql = require('./sql');

module.exports = migrate;
