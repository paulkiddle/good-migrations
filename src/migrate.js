async function migrate(storage, migrations, minVersion=0) {
	let version = await storage.getVersion() || minVersion;

	if(version < minVersion) {
		throw new TypeError(
			`Don't know how to migrate from current version (${version}) to minimum version (${minVersion}).
			You may have to downgrade to a previous version of the codebase before running migrations.`
		);
	}

	// Just check we can actually write the version before we start modifying data
	// in case an error occurs and we forget where we have gotten to.
	await storage.setVersion(version);

	try {
		// Run as many migrations as we can/as we know about.
		for(let i=version-minVersion; i < migrations.length; i++) {
			// Assuming migrations are functions with no args.
			await migrations[i]();
			version++;
		}
	} finally {
		// Record the version we got to whether or not there's an error,
		// since we still changed data along the way.
		await storage.setVersion(version);
	}
}

module.exports = migrate;
