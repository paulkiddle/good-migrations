const migrate = require('./migrate');

async function migrateSql(sql, key, migrations, options) {
	// options = Object.assign({
	// 	startIndex: 0,
	// 	//tablePrefix: ''
	// }, options);

	//const migrationsTable = options.tablePrefix + 'migrations';
	const storage = {
		async getVersion(){
			const rows = await (sql`select version from good_migrations where key=${key}`);
			return rows && rows[0] && rows[0].version;
		},
		async setVersion(version){
			const v = await this.getVersion();
			if(v == null) {
				return (sql`insert into good_migrations (key, version) values (${key}, ${version});`);
			} else {
				return (sql`update good_migrations set version=${version} where key=${key};`);
			}
		},
	}

	migrations = migrations.map(m => {
		if(typeof m === 'string') {
			return ()=>sql([m])
		}

		// if(Array.isArray(m)) {
		// 	return ()=>sql(...m);
		// }

		return m;
	});

	// This operation is idempodent, so we can run it every time without side effects
	await sql`create table if not exists good_migrations (key varchar(255) primary key, version integer)`;

	return migrate(storage, migrations, options);
}

module.exports = migrateSql;
