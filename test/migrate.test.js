const migrate = require('../src/migrate.js');

test('Migrations run', async ()=>{
	const migrations = [
		jest.fn()
	];

	const storage = {
		getVersion(){},
		setVersion: jest.fn()
	}

	await migrate(storage, migrations);

	expect(migrations[0]).toHaveBeenCalled();
	expect(storage.setVersion).toHaveBeenCalledWith(1);
})

test('Migrations run from current version', async ()=>{
	const migrations = [
		jest.fn(),
		jest.fn()
	];

	const storage = {
		getVersion(){ return 1; },
		setVersion: jest.fn()
	}

	await migrate(storage, migrations);

	expect(migrations[0]).not.toHaveBeenCalled();
	expect(migrations[1]).toHaveBeenCalled();
	expect(storage.setVersion).toHaveBeenCalledWith(2);
})

test('Saves versions if errors throw', async ()=>{
	const e = new Error;
	const migrations = [
		jest.fn(),
		()=>{throw e}
	];

	const storage = {
		getVersion(){ },
		setVersion: jest.fn()
	}

	await expect(migrate(storage, migrations)).rejects.toThrow(e);

	expect(migrations[0]).toHaveBeenCalled();
	expect(storage.setVersion).toHaveBeenCalledWith(1);
})

test('Runs migrations with min version set', async ()=>{
	const migrations = [
		jest.fn()
	];

	const storage = {
		getVersion(){ },
		setVersion: jest.fn()
	}

	await migrate(storage, migrations, 69);

	expect(migrations[0]).toHaveBeenCalled();
	expect(storage.setVersion).toHaveBeenCalledWith(70);
});


test('Throws error if current version is below min version', async ()=>{
	const migrations = [
		jest.fn(),
	];

	const storage = {
		getVersion(){ return 68; },
		setVersion: jest.fn()
	}

	await expect(migrate(storage, migrations, 69)).rejects.toThrow();

	expect(storage.setVersion).not.toHaveBeenCalled();
});
