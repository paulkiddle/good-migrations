const sql = require('sql-template-tag').default;
const sqlite3 = require('sqlite3').verbose();
const migrate = require('../src/sql.js');

function getQuery() {
	var db = new sqlite3.Database(':memory:');

	const query = jest.fn(({text, values})=> new Promise((resolve, reject) => {
		db.all(text, values, (e, value) => {
			e ? reject(e) : resolve(value)
		})
	}));
	query.sql = (strings, ...values) => {
		const s = sql(strings, ...values);
		return query(s);
	};
	return query;
}

test('Creates table', async ()=>{
	const query = getQuery();
	const migrations = [];

	await migrate(query.sql, 'key', migrations);

	expect(query).toHaveBeenCalledWith(sql`create table if not exists good_migrations (key varchar(255) primary key, version integer)`);
})

test('Sql migrations run', async ()=>{
	const query = getQuery();
	const migrations = [
		jest.fn()
	];

	await migrate(query.sql, 'key', migrations);

	expect(migrations[0]).toHaveBeenCalled();
	expect(query).toHaveBeenCalledWith(sql`insert into good_migrations (key, version) values (${'key'}, ${0});`);
	expect(query).toHaveBeenCalledWith(sql`update good_migrations set version=${1} where key=${'key'};`);
});

test('Executes sql statements', async ()=>{
	const query = getQuery();
	const migrations = [
		`SELECT * FROM good_migrations`
	];

	await migrate(query.sql, 'key', migrations);

	expect(query).toHaveBeenCalledWith(sql`SELECT * FROM good_migrations`);
});
